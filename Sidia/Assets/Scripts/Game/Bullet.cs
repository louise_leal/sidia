﻿// This script manager the bullet life time and collision
using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	[SerializeField]
	Rigidbody rigidbody;

	Coroutine bulletLife;

	private float bulletSpeed = 50f;

	void Start(){
		FireBullet ();
	}

	public void FireBullet(){
		rigidbody.velocity =  Vector3.right * bulletSpeed;
		bulletLife = StartCoroutine (LifeTime ());	
	}

	//All bullets will be destroy after 5 sesound 
	IEnumerator LifeTime(){	
		yield return new WaitForSeconds (5);
		Destroy (this.gameObject);
	}

	void OnTriggerEnter(){
		if (bulletLife != null) {
			StopCoroutine (bulletLife);
		}
		Destroy (this.gameObject);
	}
}
