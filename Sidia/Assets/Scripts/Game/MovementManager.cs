﻿using UnityEngine;
using System.Collections;

public class  MovementManager : MonoBehaviour {

	float speed = 5;
	
	// Update is called once per frame
	void Update () {
		//automatic  movement  
		transform.Translate (Time.deltaTime * speed, 0, 0);
	}

	public void GetSpeed(float newSpeed){
		speed = newSpeed;
		transform.Translate (Time.deltaTime * speed, 0, 0);
	}
}
