﻿//Shader that can define the order of the render
//Color properties is a default unity shader based this shader
Shader "QueueRender"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		ZWrite Off
		Tags {"Queue" = "Background" "LightMode"="ForwardBase"}
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				fixed4 diff : COLOR0;
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			fixed4 _Color;

			fixed4 frag (v2f i) : Color
			{
				
				return _Color;
			}
			ENDCG
		}

	}
}
