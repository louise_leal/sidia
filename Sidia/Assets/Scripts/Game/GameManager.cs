﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	[SerializeField]
	Text scoreText;

	[SerializeField]
	Text maxScore;

	[SerializeField]
	GameObject EndGameOverlay;


	//Static instance of GameManager which allows it to be accessed by any other script.
	public static GameManager instance = null;      

	int score = 0;
	float gameSpeed = 5;


	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if (instance == null)

			//if not, set instance to this
			instance = this;

		//If instance already exists and it's not this:
		else if (instance != this)

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    


		//Guarantee that the overlay is not active
		EndGameOverlay.SetActive (false);

		//Pause de game
		Time.timeScale = 0;


		scoreText.text = "Score: " + score;
		//Get the best score stored
		maxScore.text = "Best score: " + PlayerPrefs.GetInt ("MaxScore");
	}

	// Update is called once per frame
	void Update () {
		//automatic  movement  
		transform.Translate (Time.deltaTime * gameSpeed, 0, 0);
	}

	//Called when the player falls 
	public void EndGame(){
		// Store the best score player
		if (PlayerPrefs.GetInt ("MaxScore") < score) {
			PlayerPrefs.SetInt ("MaxScore", score);
		}
		//Pause game
		Time.timeScale = 0;
		//Active overlay with options de end game
		EndGameOverlay.SetActive (true);

	}


	public void StartGame(){
		//Make game run in normal time
		Time.timeScale = 1;
	}

	//Called when a box is destroy by a bullet 
	public void AddScore(string name){
		// Normal boxes give less points
		if (name == "normalBox") {
			score += 100;
		}else if(name == "specialBox"){
			score += 500;
		}

		// If score is multiple of 2000 add on the speed
		if(score/1000 == 0){
			AddSpeed(1.075f);
		}

		scoreText.text = "Score: " + score;

	}



	// Add on game speed 
	// Game speed change with score
	public void AddSpeed(float newSpeed){
		gameSpeed = gameSpeed * newSpeed;
	}

}
