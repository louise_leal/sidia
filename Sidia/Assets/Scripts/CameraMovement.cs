﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Make camera movement with the accelerometer device
public class CameraMovement : MonoBehaviour {

	[SerializeField]
	Transform target;

	void Update () {
		//Force camera look at object
		transform.LookAt(target);
		//Move camera in x and z axis 
		transform.Translate (Input.acceleration.x, 0, Input.acceleration.z * 0.05f);
	}
}
