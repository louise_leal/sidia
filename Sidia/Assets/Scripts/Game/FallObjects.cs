﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Destroy object that can fall (player and boxes)
public class FallObjects : MonoBehaviour {

	int minY = -10;
	
	// Update is called once per frame
	void Update () {
		//Check if fall to much
		if (transform.position.y <= minY) {
			DestroySelf();
		}
	}

	//Destroy itself is override in player's script
	public virtual void DestroySelf(){
		Destroy (this.gameObject);
	}
}
