﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


//Script that manager scene change and quit application
public class ApplicationManager : MonoBehaviour {


	//Function to be called by buttons to quit applucation
	public void QuitApplication(){
		Application.Quit ();
	}

	//Function to be called by buttons to load a new scene by name
	public void LoadNewScene(string sceneName){
		SceneManager.LoadScene (sceneName);
	}
}
