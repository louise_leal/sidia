﻿//Vertex animation shader and color vertex shader
Shader "VexterAnimation"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float3 vertexColor : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				fixed color : COLOR;
			};

			v2f vert (appdata v)
			{
				v2f o;
				//Move vertex as the equation producing wave like animation
				v.vertex.xyz += v.normal * (sin(v.vertex.x * 1.2 + _Time) + cos (v.vertex.z * 0.5+ _Time));
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.color = v.vertexColor;
				return o;
			}
			
			sampler2D _MainTex;
			fixed4 _Color;


			fixed4 frag (v2f i) : SV_Target
			{
				//Mix texture with color property
				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				return col;
			}
			ENDCG
		}
	}
}
