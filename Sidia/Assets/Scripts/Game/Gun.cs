﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

	[SerializeField]
	GameObject bullet;

	[SerializeField]
	ParticleSystem particle;

	// This offset is for the bullet be created outside the gun
	float bulletOffset = 0.5f;


	//Function called by UI button
	public void FireGun(){
		//Show gun's particle system
		particle.Play ();
		//Create new bullet with gun position
		Instantiate (bullet,new Vector3(transform.parent.position.x + bulletOffset, transform.parent.position.y),Quaternion.identity);
	}
}
