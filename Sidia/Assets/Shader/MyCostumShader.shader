//Shader that render two diffuses texture and a normal map
Shader "MyShader" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_SecondTex("SecondText",2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM

		#pragma surface surf Lambert

		struct Input {
				float2 uv_MainTex;
		 		float2 uv2_SecondTex;
				float2 uv_BumpMap;
		};
		sampler2D _MainTex;
		sampler2D _SecondTex;
		sampler2D _BumpMap;

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) + tex2D(_SecondTex, IN.uv2_SecondTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
		ENDCG
	}
	Fallback "Diffuse"
}
