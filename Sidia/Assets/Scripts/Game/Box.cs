﻿using UnityEngine;
using System.Collections;

public class Box : FallObjects {

	// This function detect collision with triggers
	void OnTriggerEnter(Collider colider){
		// The only case we going to treat is collision with bullets
		if (colider.gameObject.tag == "bullet") {
			GameManager.instance.AddScore (gameObject.tag.ToString ());
			Destroy (this.gameObject);
		}
	}
}
