﻿using UnityEngine;
using System.Collections;

//Script that generate platform to make a endless runner shooter
public class PlatformGenerator : MonoBehaviour {

	float minPlatDistX = 10;
	float maxPlatDistX = 15;

	float minPlatDistY = -1.5f;
	float maxPlatDistY = 1.5f;

	[SerializeField]
	GameObject platform;
	[SerializeField]
	Transform generationPoint;

	private float platformWidth; 

	Queue platformPool = new Queue();

	//Box generator parameters( max number in witch platform)
	int maxNumberOfBox = 3;

	[SerializeField]
	GameObject normalBox;

	[SerializeField]
	GameObject specalBox;



	// Use this for initialization
	void Start () {
		platformWidth = platform.GetComponent<BoxCollider> ().size.x;
		//Define number max of platforms
		int maxNumberofplatform = 10;
		for (int i = 0; i < maxNumberofplatform; i++) {
			//platform Generator point moves together to the new onws creates
			transform.position = new Vector3 (transform.position.x + platformWidth + Random.Range(minPlatDistX,maxPlatDistX), transform.position.y);
			//Create new platform with rando x and y in the range 
			GameObject newplatform  = (GameObject) Instantiate (platform, new Vector3(transform.position.x, transform.position.y + Random.Range(minPlatDistY,maxPlatDistY)), Quaternion.identity);
			//Make all children be in the same container
			newplatform.transform.SetParent (transform.parent);
			//Create boxes
			BoxGenerator();
			//Put in the Queue
			platformPool.Enqueue (newplatform);
		}

	}
	
	// Update is called once per frame
	void Update () {
		
		if (transform.position.x < generationPoint.position.x) {
			//Take out last platform to change position
			GameObject changeplatform = (GameObject)platformPool.Dequeue ();
			//Platform Generator point moves together to the new owns creates
			transform.position = new Vector3 (transform.position.x + platformWidth + Random.Range (minPlatDistX, maxPlatDistX), transform.position.y);
			changeplatform.transform.position = new Vector3 (transform.position.x, transform.position.y + Random.Range (minPlatDistY, maxPlatDistY));
			//The boxes will be generator every time a platform changes place
			BoxGenerator ();
			// Put platform back on the queue;
			platformPool.Enqueue (changeplatform);

		}
	}

	// To ensure that wall boxes will be created in a platform
	void BoxGenerator(){
		int numberOfBox = Random.Range (0, maxNumberOfBox);
		GameObject newBox;
		for (int i = 0; i < numberOfBox; i++) {
			//Has 60% to be a normal box and 20 to be a special box
			if (Random.value <= 0.6f) {
				newBox = normalBox;				
			}else{
				newBox = specalBox;
			}
			// The box will be create in a rando place in the platform
			newBox = Instantiate(newBox, new Vector3(transform.position.x + Random.Range(-platformWidth/2 , platformWidth/2), transform.position.y + 5), Quaternion.identity);

			//This will guarantee that all boxes will be created and be inside parent to be more organized
			newBox.transform.SetParent (transform.parent);

		}
	}

}
