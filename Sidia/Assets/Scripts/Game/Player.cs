﻿using UnityEngine;
using System.Collections;

//Script that manager player movement and collision
public class Player : FallObjects {


	public float jumpSpeed = 0;
	public bool isGrounded;
	// Update is called once per frame
	[SerializeField]
	Rigidbody rigidbody;

	//Change gravity to jump appear better
	void Start(){
		//Change gravity to make player fall faster
		Physics.gravity = new Vector3(0, -15.0F, 0);
	}


	//Function called by UI button
	public void JumpPlayer(){
		//Jump
		if(isGrounded ){
			rigidbody.AddForce(Vector3.up * jumpSpeed);
			isGrounded = false;
		}
	}

	// Make sure only jump when on the ground
	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "platform") {
			isGrounded = true;
		}
	}

	// Function that treat end game if player fall
	public override void DestroySelf (){
		GameManager.instance.EndGame ();
		base.DestroySelf ();
	} 
}
